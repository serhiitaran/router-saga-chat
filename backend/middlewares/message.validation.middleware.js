const { message } = require('../models/message');
const { checkRequiredFields, validateFields, checkErrors } = require('../helpers/middlewareHelper');

const createMessageValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    checkRequiredFields(fields, message, errors);
    validateFields(fields, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateMessageValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    validateFields(fields, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createMessageValid = createMessageValid;
exports.updateMessageValid = updateMessageValid;
