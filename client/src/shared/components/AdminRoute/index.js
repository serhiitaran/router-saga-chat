import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const AdminRoute = ({ profile, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        profile.data && profile.data.login === 'admin' ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/chat',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};
const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(mapStateToProps)(AdminRoute);
