import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import App from './App';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { saveState } from './shared/helpers/storageHelper';

import './index.css';

const store = configureStore();

store.subscribe(() => {
  saveState({
    profile: store.getState().profile
  });
});

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('root')
);
