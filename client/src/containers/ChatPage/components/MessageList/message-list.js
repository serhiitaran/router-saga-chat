import React from 'react';

import Message from '../Message';
import './message-list.css';

export const MessageList = props => {
  const { currentUserId, messages, onMessageDelete, onMessageEdit, onMessageLike } = props;
  let messagesDate;
  const messageItems = messages.map(message => {
    const { id, date, ...messageProps } = message;
    const isSameDate = messagesDate === date;
    if (!isSameDate) {
      messagesDate = date;
    }
    const messageDateLine = (
      <div key={date} className="messages__date-line">
        <span>{date}</span>
      </div>
    );
    return (
      <React.Fragment key={id}>
        {!isSameDate && messageDateLine}
        <Message
          currentUserId={currentUserId}
          {...messageProps}
          onDelete={() => onMessageDelete(id)}
          onEdit={() => onMessageEdit(id)}
          onLike={() => onMessageLike(id, messageProps.likes, currentUserId)}
        />
      </React.Fragment>
    );
  });
  return <div className="messages">{messageItems}</div>;
};
