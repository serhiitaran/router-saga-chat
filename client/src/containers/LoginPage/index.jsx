import React from 'react';
import { connect } from 'react-redux';
import { loginUser } from './actions';
import { Redirect } from 'react-router-dom';

import LoginForm from './components/LoginForm';
import Loader from '../../shared/components/Loader';
import Error from '../../shared/components/Error';

import './index.css';

const LoginPage = props => {
  const { profile, loginUser } = props;
  return (
    <>
      {profile.error && <Error message={profile.error} />}
      {profile.data && <Redirect to="/users" />}
      <div className="login">
        {profile.isLoading ? (
          <Loader />
        ) : (
          <>
            <h3 className="login__header">Log in</h3>
            <LoginForm loginUser={loginUser} />
          </>
        )}
      </div>
    </>
  );
};

const mapStateToProps = state => ({
  profile: state.profile
});

const mapDispatchToProps = {
  loginUser
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
